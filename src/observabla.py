from tkinter import *
import jsonpickle, json
import hashlib


class Observabla:
    def __init__(self):
        self.album = StringVar()
        self.results_words_to_remember = TestResult()
        self.scenario = StringVar()
        self.results_guessed_words = TestResult()
        self.guessed_words_count = 0
        self.choice1 = IntVar(value=-1)
        self.choice2 = IntVar(value=-1)
        self.choice3 = IntVar(value=-1)
        self.choiceR = IntVar(value=-1)

    def words_guessed(self, guessed_words):
        self.guessed_words_count = len(guessed_words)

    def add_result(self, tr, guessed_words, time, test_name):
        tr.guessed_words = len(guessed_words)
        tr.time = time
        tr.test_name = test_name

    def add_result_words_to_remember(self, guessed_words, time, test_name):
        tr = self.results_words_to_remember
        self.add_result(tr, guessed_words, time, test_name)

    def add_result_guessed_words(self, guessed_words, time, test_name):
        tr = self.results_guessed_words
        self.add_result(tr, guessed_words, time, test_name)

    def get_results(self):
        return "\n".join([
            str(self.results_words_to_remember.__dict__),
            str(self.results_guessed_words.__dict__)
        ])

    def get_answers(self):
        return "\n answer 1: {}, answer :2 {}, answer 3 {}, answer R: {}".format(
            self.choice1.get(), self.choice2.get(), self.choice3.get(), self.choiceR.get())

    def __repr__(self):
        return "album {}\nscenario: {}\nguessed words: {}\nresults: {}\nanswers {}\n".format(
            self.album.get(), self.scenario.get(), self.guessed_words_count, self.get_results(), self.get_answers()
        )

    def to_json(self):
        data = {
            'album' : self.album.get(),
            'results_words_to_remember_time': self.results_words_to_remember.time.total_seconds(),
            'scenario' : self.scenario.get(),
            'results_guessed_words_time' : self.results_guessed_words.time.total_seconds(),
            'guessed_words_count' : self.guessed_words_count,
            'choice1' : self.choice1.get(),
            'choice2' : self.choice2.get(),
            'choice3' : self.choice3.get(),
            'choiceR' : self.choiceR.get(),
        }
        hash_data = json.dumps(data, sort_keys=True)
        data['result_hash'] = hashlib.md5(hash_data.encode('utf-8')).hexdigest()
        return jsonpickle.encode(data)

    @staticmethod
    def load_result_from_file(file):
        with open(file, 'r') as infile:
            data = infile.readlines()
            data = jsonpickle.decode(data[0])
            return data


class TestResult:
    def __init__(self):
        self.guessed_words = None
        self.time = None
        self.test_name = None

    def __repr__(self):
        return str("guessed words: {}, time {}, test name {} ".format(self.guessed_words, self.time.total_seconds(), self.test_name))


