import jsonpickle
import os
from observabla import Observabla
from observabla import TestResult
import hashlib, json
import csv

with open('experyment1.csv', 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter=' ',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    csvwriter.writerow(['album', 'scenario', 'choice1', 'choice2', 'choice3', 'choiceR', 'guessed_words_count',  'results_words_to_remember_time', 'results_guessed_words_time'])

    files = [f for f in os.listdir('.') if (os.path.isfile(f) and f.endswith('.json'))]
    for f in files:
        print(f)
        data = Observabla.load_result_from_file(f)
        hash_string = {
            'album': data['album'],
            'results_words_to_remember_time': data['results_words_to_remember_time'],
            'scenario': data['scenario'],
            'results_guessed_words_time': data['results_guessed_words_time'],
            'guessed_words_count': data['guessed_words_count'],
            'choice1': data['choice1'],
            'choice2': data['choice2'],
            'choice3': data['choice3'],
            'choiceR': data['choiceR'],
        }
        hash_data = json.dumps(hash_string, sort_keys=True)
        hash = hashlib.md5(hash_data.encode('utf-8')).hexdigest()
        print('compare hash:', str(hash) == str(data['result_hash']))

        csvwriter.writerow([hash_string['album'],
                            hash_string['scenario'],
                            hash_string['choice1'],
                            hash_string['choice2'],
                            hash_string['choice3'],
                            hash_string['choiceR'],
                            hash_string['guessed_words_count'],
                            hash_string['results_words_to_remember_time'],
                            hash_string['results_guessed_words_time']])