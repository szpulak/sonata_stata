from os import path
import sys
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import smtplib
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from tkinter.scrolledtext import ScrolledText
import copy
import datetime
from data import zestaw_4, passwd, scenariusze_punkty, R_scenariusze, slajd16, slajd_sent, podzial, mapa_podzial
import os
from observabla import Observabla
import keyboard
import platform
import random


MEDIUM_FONT = ("Verdana", 10)
LARGE_FONT = ("Verdana", 12)
VERY_LARGE_FONT = ("Verdana", 16)

pressed_f4 = False

dzien = 2

class StataQuiz():

    def __init__(self, *args, **kwargs):
        self.root = Tk()

        # self.root.protocol("WM_DELETE_WINDOW", self.do_exit)
        self.root.wm_attributes("-fullscreen", True)

        plt = platform.system()
        if plt == "Windows":
            keyboard.unhook_all()
            keyboard.add_hotkey('alt+f4', lambda: None, suppress=True)
            keyboard.add_hotkey('alt+tab', lambda: None, suppress=True)
            keyboard.add_hotkey('win', lambda: None, suppress=True)
            keyboard.add_hotkey('left win', lambda: None, suppress=True)
            keyboard.add_hotkey('right win', lambda: None, suppress=True)
            keyboard.add_hotkey('prtscn', lambda: None, suppress=True)
            keyboard.add_hotkey('prnt scrn', lambda: None, suppress=True)
            keyboard.add_hotkey('snapshot', lambda: None, suppress=True)

        self.root.title("Eksperyment #1")

        self.user = Observabla()
        self.generate_words()

        container = Frame(self.root)
        container.pack(side="top", fill="both", expand=True, padx=30, pady=30)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        self.frames[StartPage] = StartPage(container, self)
        self.frames[WordsToRemeberPage] = WordsToRemeberPage(container, self, 60 *60)
        self.frames[EnterWordsPage] = EnterWordsPage(container, self, 60 * 10)

        self.frames[InfoPage0] = InfoPage0(container, self)

        self.frames[InfoPage1] = InfoPage1(container, self)
        self.frames[Table1Page] = Table1Page(container, self)
        self.frames[QuestionPage1] = QuestionPage1(container, self,
                                                   self.user.choice1)

        self.frames[QuestionPage2] = QuestionPage2(container, self,
                                                   self.user.choice2)
        self.frames[InfoPage2] = InfoPage2(container, self)
        self.frames[Table2Page] = Table2Page(container, self)

        self.frames[QuestionPage3] = QuestionPage3(container, self,
                                                   self.user.choice3)
        self.frames[InfoPage3] = InfoPage3(container, self)
        self.frames[Table3Page] = Table3Page(container, self)

        self.frames[QuestionPageR] = QuestionPageR(container, self,
                                                   self.user.choiceR)
        self.frames[InfoPageR] = InfoPageR(container, self)
        self.frames[TableRPage] = TableRPage(container, self)

        self.frames[InfoPage16] = InfoPage16(container, self)
        self.frames[InfoPage17] = InfoPage17(container, self)

        self.frames[SentPage] = SentPage(container, self)

        for F in self.frames.values():
            F.grid(row=0, column=0, columnspan=10, rowspan=10, sticky="nsew")

        self.show_frame(StartPage)
        self.root.mainloop()

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.event_generate("<<ShowFrame>>")
        frame.tkraise()

    def generate_words(self):
        random.seed()
        self.word_list = random.sample(zestaw_4, 50)

    def do_exit(self):
        pass


class StartPage(Frame):

    pas = passwd[dzien]

    def __init__(self, parent, controller):
        super().__init__(parent)

        for row in range(2):
            self.grid_rowconfigure(row, weight=1)
            self.grid_columnconfigure(row, weight=1)
        for row in range(5):
            self.grid_rowconfigure(row, weight=1)

        Label(self, text="Numer albumu", font=VERY_LARGE_FONT, relief=RIDGE, width=40).grid(row=1, column=0)

        self.entry1 = Entry(self, font=VERY_LARGE_FONT, relief=SUNKEN, width=40, textvariable=controller.user.album)
        self.entry1.grid(row=1, column=1, rowspan=1, columnspan=1)

        Label(self, text="Haslo", font=VERY_LARGE_FONT, relief=RIDGE, width=40).grid(row=2, column=0)
        entrypass = StringVar()
        self.entry2 = Entry(self, font=VERY_LARGE_FONT, relief=SUNKEN, width=40, textvariable=entrypass)
        self.entry2.grid(row=2, column=1, rowspan=1, columnspan=1)

        Label(self, text="Scenariusz:", font=VERY_LARGE_FONT, relief=RIDGE, width=40).grid(row=3)

        self.ScenarioSelector = ttk.Combobox(self, font=VERY_LARGE_FONT, width=40,
                                             justify='center', state='readonly',
                                             values=sorted(scenariusze_punkty.keys()),
                                             textvariable=controller.user.scenario
                                             )
        self.ScenarioSelector.grid(row=3, column=1, rowspan=1, columnspan=1)
        self.ScenarioSelector.current(0)
        self.ScenarioSelector.configure(state='disabled')

        button = Button(self, text="Dalej", font=VERY_LARGE_FONT,
                           command=lambda: self.next_page(controller, entrypass),
                           padx=20, pady=20
                           )
        button.grid(row=4, column=1, sticky=W)

        button2 = Button(self, text="Wyjdź", font=VERY_LARGE_FONT,
                            command=lambda: controller.root.destroy(),
                            padx=20, pady=20)
        button2.grid(row=4, column=0, sticky=E)

    def next_page(self, controller, entrypass):
        controller.root.focus()
        try:
            if not(isinstance(entrypass.get(), str) and entrypass.get().lower() == self.pas.lower()):
                messagebox.showinfo("Error", "Błędne hasło")
                return
            user_scenario = podzial.get(int(controller.user.album.get()))
            if user_scenario:
                messagebox.showinfo("Info", "Twój scenariusz to: {}".format(user_scenario))
                self.ScenarioSelector.current(mapa_podzial.get(user_scenario))
                controller.show_frame(InfoPage0)
            else:
                messagebox.showinfo("Error", "Problem z wyborem scenariusza")
            if controller.user.scenario.get() != 'D':
                self.event_generate("<<ScenarioReady>>")
                self.event_generate("<<ScenarioReady2>>")
                self.event_generate("<<ScenarioReady3>>")
        except Exception as e:
            messagebox.showinfo("Error", str(e))

class SentPage(Frame):

    def __init__(self, parent, controller):
        super().__init__(parent)

        self.controller = controller

        for row in range(5):
            self.grid_rowconfigure(row, weight=1)
            #self.grid_columnconfigure(row, weight=1)

        self.wynik_label = Label(
            self, text="Twoj wynik to: {} słów poprawnie zapamietanych. Pamiętaj, jeśli się ubezpieczyłeś i chcesz skorzystać z ubezpieczenia musisz złożyć pisemny wniosek na konsultacjach.\n \
            DZIĘKUJEMY ZA UDZIAŁ W EKSPERYMENCIE!".format("0"),
            font=LARGE_FONT,
            width=75)
        self.wynik_label.grid(row=0, column=0)

        self.st = ScrolledText(self, state='normal')
        self.st.configure(font=LARGE_FONT, height=10, width=80)
        self.st.grid(row=3, column=0)

        button1 = Button(self, text="Wyslij wynik z wynikami do prowadzacego zajecia", font=LARGE_FONT,width=75,
                            command=lambda: self.send_email())
        button1.grid(row=1, column=0, rowspan=2)

        button2 = Button(self, text="Wyjdź", font=LARGE_FONT, width=75,
                            command=lambda: controller.root.destroy())
        button2.grid(row=4, column=0)

        self.bind("<<ShowFrame>>", self.sent_result)

    def save_json(self):
        filename = "{}.json".format(self.controller.user.album.get())
        with open(filename, 'w') as outfile:
            outfile.write(self.controller.user.to_json())

        return os.path.abspath(filename)

    def send_email(self):
        email = EmailSender()

        try:
            email.send_message_owner(self.controller.user)
            email.send_message_student(self.controller.user)
            logging_mgs = "email sent !"
        except Exception as e:
            logging_mgs = str(e)

        if self.st:
            self.st.configure(state='normal')
            self.st.insert(END, logging_mgs + '\n')
            self.st.yview(END)
            self.st.configure(state='disabled')

    def show_me(self):
        self.tkraise()
        self.event_generate("<<ShowFrame>>")

    def sent_result(self, *args):
        text = "Twoj wynik to: {} słów poprawnie zapamietanych".format(self.controller.user.guessed_words_count)


        self.wynik_label.configure(text=text)
        self.st.configure(state='normal')
        self.st.insert(END, str(self.controller.user) + '\n')
        path = self.save_json()
        self.st.insert(END, 'Plik z wynikami zapisano w: ' + path + '\n')
        self.st.yview(END)
        self.st.configure(state='disabled')
        self.send_email()


class WordsPage(Frame):
    def __init__(self, parent, controller, seconds_on_page=5):
        super().__init__(parent)
        for row in range(12):
            self.grid_rowconfigure(row, weight=1)
        for row in range(6):
            self.grid_columnconfigure(row, weight=1)

        button2 = Button(self, text="Dalej", font=VERY_LARGE_FONT, padx=20, pady=20,
                            command=lambda: self.next_page(controller))
        button2.grid(row=12, column=3, sticky=W)

        self.timer_running = False
        self.stopwatch_time = datetime.timedelta(seconds=seconds_on_page)
        self.stopwatch = Label(self, text=str(self.stopwatch_time), font=LARGE_FONT)
        self.stopwatch.grid(row=11, column=4)

        self.stopwatch.after(1000, self.sub_time, controller)

    def next_page(self, controller):
        pass

    def show_me(self):
        self.event_generate("<<ShowFrame>>")
        self.tkraise()

    def enable_timer(self):
        self.timer_running = True

    def disable_timer(self):
        self.timer_running = False

    def sub_time(self, controller):
        if self.timer_running:
            self.stopwatch_time = max(
                self.stopwatch_time - datetime.timedelta(seconds=1),
                datetime.timedelta(seconds=0)
            )
            if self.stopwatch_time.total_seconds() <= 0:
                self.next_page(controller)
            self.stopwatch.configure(text=str(self.stopwatch_time))
        self.stopwatch.after(1000, self.sub_time, controller)


class WordsToRemeberPage(WordsPage):

    def __init__(self, parent, controller, seconds_on_page):
        super().__init__(parent, controller, seconds_on_page)

        data_copy = copy.copy(controller.word_list)

        for x in range(5):
            for y in range(10):
                Label(self, text=data_copy.pop().upper(), font=LARGE_FONT).grid(row=y, column=x)

    def next_page(self, controller):
        self.disable_timer()
        controller.user.add_result_words_to_remember([], self.stopwatch_time, 'Nauka slow')
        controller.show_frame(InfoPage17)


class EnterWordsPage(WordsPage):
    def __init__(self, parent, controller, seconds_on_page):
        super().__init__(parent, controller, seconds_on_page)

        self.entry_word_list = []

        for x in range(5):
            for y in range(10):
                entry = Entry(self, font=LARGE_FONT, relief=SUNKEN, width=20)
                entry.grid(row=y, column=x)
                self.entry_word_list.append(entry)

    def next_page(self, controller):
        self.disable_timer()
        guessed_words_list = []
        for entry in self.entry_word_list:
            guessed_words_list.append(entry.get().upper())
        word_list = [word.upper() for word in controller.word_list]
        guessed_words_list = set(word_list) & set(guessed_words_list)
        controller.user.add_result_guessed_words(guessed_words_list, self.stopwatch_time , 'Wypelnianie slow')
        controller.user.words_guessed(guessed_words_list)
        next_frame = controller.frames[SentPage]
        next_frame.show_me()


class InfoPage(Frame):
    def __init__(self, parent, controller):
        self.controller = controller
        super().__init__(parent)
        for row in range(4):
            self.grid_rowconfigure(row, weight=1)

        for row in range(4):
            self.grid_columnconfigure(row, weight=1)

        self.st = Text(self, state='normal', font=LARGE_FONT, height=20, width=80, relief=GROOVE, wrap=WORD)
        self.st.grid(row=0, columnspan=2, rowspan=3)

        self.button_next = Button(self, text="Dalej",font=VERY_LARGE_FONT, padx=20, pady=20,
                            command=lambda: self.next_page(controller))
        self.button_next.grid(row=3, column=1, sticky=W)

        button3 = Button(self, text="Wyjdź", font=VERY_LARGE_FONT,
                            command=lambda: controller.root.destroy(),
                            padx=20, pady=20)
        button3.grid(row=3, column=0, sticky=E)

        # self.button_previous = Button(self, text="Wstecz",font=VERY_LARGE_FONT, padx=20, pady=20,
        #                     command=lambda: self.previous_page(controller))
        # self.button_previous.grid(row=3, column=0, sticky=E)

    def next_page(self, controller):
        pass

    def previous_page(self, controller):
        pass

    def show_me(self):
        self.event_generate("<<ShowFrame>>")
        self.tkraise()


class QuestionPage(InfoPage):
    def __init__(self, parent, controller, choice_var):
        super().__init__(parent, controller)
        self.controller = controller
        self.choice_var = choice_var
        self.button_next.configure(state='disabled')

    def enable_next_button(self, *args):
        self.button_next.configure(state='normal')

    def next_page(self, controller):
        pass

    def previous_page(self, controller):
        pass


class TablePage(Frame):
    def __init__(self, parent, controller):
        super().__init__(parent)
        self.controller = controller

        for row in range(29):
            self.grid_rowconfigure(row, weight=1)

        for row in range(8):
            self.grid_columnconfigure(row, weight=1)

        text1 = "Zapamiętane\nsłowa"
        text2 = "Twój\nwynik\nkoncowy"

        self.header1 = Label(self, text="JEŚLI NIE KUPISZ UBEZPIECZENIA", font=LARGE_FONT)
        self.header1.grid(row=0, column=0, columnspan=4)
        self.header2 = Label(self, text="JEŚLI KUPISZ UBEZPIECZENIE", font=LARGE_FONT)
        self.header2.grid(row=0, column=4, columnspan=4)

        Label(self, text=text1, font=MEDIUM_FONT).grid(row=1, column=0)
        Label(self, text=text2, font=MEDIUM_FONT).grid(row=1, column=1)
        Label(self, text=text1, font=MEDIUM_FONT).grid(row=1, column=2)
        Label(self, text=text2, font=MEDIUM_FONT).grid(row=1, column=3)

        Label(self, text=text1, font=MEDIUM_FONT).grid(row=1, column=4)
        Label(self, text=text2, font=MEDIUM_FONT).grid(row=1, column=5)
        Label(self, text=text1, font=MEDIUM_FONT).grid(row=1, column=6)
        Label(self, text=text2, font=MEDIUM_FONT).grid(row=1, column=7)

        for i in range(1, 27):
            Label(self, text=str(51 - i), font=MEDIUM_FONT).grid(row=i+2, column=0)
            Label(self, text=str(0), font=MEDIUM_FONT).grid(row=i + 2, column=1)

        for i in range(1, 21):
            Label(self, text=str(42 - 2*i), font=MEDIUM_FONT).grid(row=i+2, column=1)

        for i in range(1, 26):
            Label(self, text=str(25-i), font=MEDIUM_FONT).grid(row=i+2, column=2)
            Label(self, text=str(0), font=MEDIUM_FONT).grid(row=i + 2, column=3)

        for i in range(1, 27):
            Label(self, text=str(51-i), font=MEDIUM_FONT).grid(row=i+2, column=4)
            Label(self, text=str(0), font=MEDIUM_FONT).grid(row=i + 2, column=5)

        for i in range(1, 26):
            Label(self, text=str(25-i), font=MEDIUM_FONT).grid(row=i+2, column=6)
            Label(self, text=str(0), font=MEDIUM_FONT).grid(row=i + 2, column=7)

        ttk.Separator(self, orient=VERTICAL).grid(column=1, row=2, rowspan=27, sticky='nse')
        ttk.Separator(self, orient=VERTICAL).grid(column=3, row=0, rowspan=29, sticky='nse')
        ttk.Separator(self, orient=VERTICAL).grid(column=5, row=2, rowspan=27, sticky='nse')

        self.button_next = Button(self, text="Dalej", font=VERY_LARGE_FONT, padx=20, pady=20,
                            command=lambda: self.next_page(controller))
        self.button_next.grid(row=29, column=4, sticky=W)

        # self.button_previous = Button(self, text="Wstecz",font=VERY_LARGE_FONT, padx=20, pady=20,
        #                     command=lambda: self.previous_page(controller))
        # self.button_previous.grid(row=29, column=3, sticky=E)

        button3 = Button(self, text="Wyjdź", font=VERY_LARGE_FONT,
                            command=lambda: controller.root.destroy(),
                            padx=20, pady=20)
        button3.grid(row=29, column=3, sticky=E)

    def fill_custom_data(self):
        pass

    def next_page(self, controller):
        pass

    def previous_page(self, controller):
        pass


class InfoPage0(InfoPage):

    def next_page(self, controller):
        if controller.user.scenario.get() != 'D':
            controller.show_frame(InfoPageR)
        else:
            controller.show_frame(InfoPage1)

    def previous_page(self, controller):
        controller.show_frame(StartPage)

    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.st.delete(1.0, END)
        self.st.insert(END, "Szanowny Studencie,\n\n"
                            
                            "W ramach eksperymentu, w którym zgodziłeś się uczestniczyć, "
                            "weźmiesz teraz udział w grze, która polega na tym, "
                            "by zapamiętać 50 polskich słów. Na ich zapamiętanie możesz "
                            "poświęcić najwyżej 60 minut. Natomiast na spisanie "
                            "zapamiętywanych słów możesz przeznaczyć najwyżej 10 minut. "
                            "Uwaga, jeśli przerzucisz slajd ze słowami do zapamiętania "
                            "nie będziesz już mógł do niego wrócić!!!.\n\n"
                            
                            "Na początku gry otrzymujesz 40 punktów. Za każde "
                            "niezapamiętane lub błędnie wpisane słowo w grze stracisz "
                            "2 punkty. W zależności od tego, ile słów zapamiętasz, z taką "
                            "liczbą punktów zakończysz grę. \n\n"
                            "UWAGA! Na kilku pierwszych slajdach znajduje się przycisk WYJDŹ, "
                            "ale odradzamy kilkanie w niego. \n\n"
                            "DODATKOWO PRZYPOMINAMY O NIEWPISYWANIU SPACJI W TABELCE DO WPISYWANIA SŁÓW " )

        self.st.configure(state='disabled')


class InfoPage1(InfoPage):

    def next_page(self, controller):
        controller.show_frame(Table1Page)

    def previous_page(self, controller):
        controller.show_frame(InfoPage0)

    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.st.delete(1.0, END)
        self.st.insert(END, "Zastanów się, czy byłbyś skłonny wykupić ubezpieczenie "
                            "od utraty punktów, które kosztuje 3 punkty i w pełni "
                            "chroni Cię przed utratą punktów w razie niezapamiętania "
                            "dowolnej liczby słów? Wybierając grę z tym ubezpieczeniem zawsze otrzymałbyś 37 punktów, niezależnie od tego, ile słów byś zapamiętał. Przykładowo: jeśli nie zapamiętałbyś 10 słów to grając z tym ubezpieczeniem straciłbyś 0 punktów, grając bez ubezpieczenia starciłbyś 20 punktów." \
                            "\n"
                            "Na następnym slajdzie zobaczysz, "
                            "jak kształtowałaby się liczba Twoich punktów w sytuacji, "
                            "gdy nie wykupiłbyś i gdybyś wykupił ubezpieczenie. "
                            "Potem odpowiesz na zadane pytanie. "
                            "\n" "\n"
                            "Jeżeli wykupiłbyś ubezpieczenie i okazałoby się ono "
                            "przydatne musiałbyś złożyć wniosek on-line (mailem) "
                            "o jego zrealizowanie według dostępnego wzoru "
                            "w ciągu 1 tygodnia od zakończenia gry. "
                            "Jeśli tego byś nie zrobił, ubezpieczenie nie zadziałałoby." )
        self.st.configure(state='disabled')


class Table1Page(TablePage):
    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.header1.configure(text="JEŚLI NIE KUPIŁBYŚ UBEZPIECZENIA")
        self.header2.configure(text="JEŚLI KUPIŁBYŚ UBEZPIECZENIE", )
        self.fill_custom_data()

    def fill_custom_data(self, *args):

        punktacja = scenariusze_punkty.get('A')

        for i in range(0, 26):
            Label(self, text=str(punktacja[i]), font=MEDIUM_FONT).grid(row=i + 3, column=5)

        for i in range(0, 25):
            Label(self, text=str(punktacja[26+i]), font=MEDIUM_FONT).grid(row=i + 3, column=7)

    def next_page(self, controller):
        controller.show_frame(QuestionPage1)

    def previous_page(self, controller):
        controller.show_frame(InfoPage1)


class QuestionPage1(QuestionPage):

    def next_page(self, controller):
        controller.show_frame(InfoPage2)

    def __init__(self, parent, controller, choice_var):
        super().__init__(parent, controller, choice_var)
        rb = Radiobutton(self, text="TAK", font=LARGE_FONT, variable=self.choice_var, value=1, command=self.enable_next_button, indicatoron=0, height=4, width=8).grid(row=2, column=0)
        rb = Radiobutton(self, text="NIE", font=LARGE_FONT, variable=self.choice_var, value=0, command=self.enable_next_button, indicatoron=0, height=4, width=8).grid(row=2, column=1)
        self.st.insert(END, "Odpowiedz teraz, czy hipotetycznie byłbyś skłonny wykupić "
                            "ubezpieczenie od utraty punktów, które kosztuje 3 punkty "
                            "i w pełni chroni Cię przed utratą punktów w razie niezapamiętania "
                            "dowolnej liczby słów?  "
                            "\n" "\n"
                            "Przypominamy, jeżeli wykupiłbyś ubezpieczenie i okazałoby się ono "
                            "przydatne musiałbyś złożyć wniosek on-line (mailem) "
                            "o jego zrealizowanie według dostępnego wzoru "
                            "w ciągu 1 tygodnia od zakończenia gry. "
                            "Jeśli tego byś nie zrobił, ubezpieczenie nie zadziałałoby." )

        self.st.configure(state='disabled')

    def previous_page(self, controller):
        controller.show_frame(Table1Page)


class InfoPage2(InfoPage):

    def next_page(self, controller):
        controller.show_frame(Table2Page)

    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.st.delete(1.0, END)
        self.st.insert(END, "Zastanów się, czy byłbyś skłonny wykupić ubezpieczenie "
                            "od utraty punktów, które kosztuje 2 punkty przy udziale  "
                            "własnym 10% (oznacza to, że za niezapamiętanie 1 słowa tracisz"
                            " 0,2 punktu a nie 2 punkty)? Przykładowo: jeśli nie zapamiętałbyś 10 słów to grając z tym ubezpieczeniem straciłbyś 2 punkty, grając bez ubezpieczenia starciłbyś 20 punktów."
                            "\n"
                            "Na następnym slajdzie zobaczysz, "
                            "jak kształtowałaby się liczba Twoich punktów w sytuacji,"
                            " gdy nie wykupiłbyś i gdybyś wykupił ubezpieczenie. "
                            "Potem odpowiesz na zadane pytanie. "
                            "\n" "\n"
                            "Jeżeli wykupiłbyś ubezpieczenie i okazałoby się ono "
                            "przydatne musiałbyś złożyć wniosek on-line (mailem) "
                            "o jego zrealizowanie według dostępnego wzoru "
                            "w ciągu 1 tygodnia od zakończenia gry. "
                            "Jeśli tego byś nie zrobił, ubezpieczenie nie zadziałałoby." )
        self.st.configure(state='disabled')

    def previous_page(self, controller):
        controller.show_frame(QuestionPage1)


class Table2Page(TablePage):
    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.header1.configure(text="JEŚLI NIE KUPIŁBYŚ UBEZPIECZENIA")
        self.header2.configure(text="JEŚLI KUPIŁBYŚ UBEZPIECZENIE", )
        self.fill_custom_data()

    def fill_custom_data(self, *args):

        punktacja = scenariusze_punkty.get('B')

        for i in range(0, 26):
            Label(self, text=str(punktacja[i]), font=MEDIUM_FONT).grid(row=i + 3, column=5)

        for i in range(0, 25):
            Label(self, text=str(punktacja[26+i]), font=MEDIUM_FONT).grid(row=i + 3, column=7)

    def next_page(self, controller):
        controller.show_frame(QuestionPage2)

    def previous_page(self, controller):
        controller.show_frame(InfoPage2)


class QuestionPage2(QuestionPage):

    def next_page(self, controller):
        controller.show_frame(InfoPage3)

    def __init__(self, parent, controller, choice_var):
        super().__init__(parent, controller, choice_var)
        rb = Radiobutton(self, text="TAK", font=LARGE_FONT, variable=self.choice_var, value=1, command=self.enable_next_button, indicatoron=0, height=4, width=8).grid(row=2, column=0)
        rb = Radiobutton(self, text="NIE", font=LARGE_FONT, variable=self.choice_var, value=0, command=self.enable_next_button, indicatoron=0, height=4, width=8).grid(row=2, column=1)
        self.st.delete(1.0, END)
        self.st.insert(END, "Odpowiedz teraz, czy hipotetycznie byłbyś skłonny "
                            "wykupić ubezpieczenie od utraty punktów, które "
                            "kosztuje 2 punkty przy udziale  własnym 10% "
                            "(oznacza to, że za niezapamiętanie 1 słowa tracisz "
                            "0,2 punktu a nie 2 punkty)?"
                            "\n" "\n"
                            "Jeżeli wykupiłbyś ubezpieczenie i okazałoby się ono "
                            "przydatne musiałbyś złożyć wniosek on-line (mailem) "
                            "o jego zrealizowanie według dostępnego wzoru "
                            "w ciągu 1 tygodnia od zakończenia gry. "
                            "Jeśli tego byś nie zrobił, ubezpieczenie nie zadziałałoby." )

        self.st.configure(state='disabled')

    def previous_page(self, controller):
        controller.show_frame(Table2Page)


class InfoPage3(InfoPage):

    def next_page(self, controller):
        controller.show_frame(Table3Page)

    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.st.delete(1.0, END)
        self.st.insert(END, "Zastanów się, czy byłbyś skłonny wykupić ubezpieczenie "
                            "od utraty punktów, które kosztuje 1 punkt przy "
                            "udziale  własnym 20% (oznacza to, że za niezapamiętanie "
                            "1 słowa tracisz 0,4 punktu a nie 2 punkty)? Przykładowo: jeśli nie zapamiętałbyś 10 słów to grając z tym ubezpieczeniem straciłbyś 4 punkty, grając bez ubezpieczenia starciłbyś 20 punktów."
                            "\n"
                            "Na następnym slajdzie zobaczysz, jak kształtowałaby się liczba Twoich "
                            "punktów w sytuacji, gdy nie wykupiłbyś i gdybyś wykupił "
                            "ubezpieczenie. Potem odpowiesz na zadane pytanie. "
                            "\n" "\n"
                            "Jeżeli wykupiłbyś ubezpieczenie i okazałoby się ono "
                            "przydatne musiałbyś złożyć wniosek on-line (mailem) "
                            "o jego zrealizowanie według dostępnego wzoru "
                            "w ciągu 1 tygodnia od zakończenia gry. "
                            "Jeśli tego byś nie zrobił, ubezpieczenie nie zadziałałoby." )
        self.st.configure(state='disabled')

    def previous_page(self, controller):
        controller.show_frame(QuestionPage2)


class Table3Page(TablePage):
    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.header1.configure(text="JEŚLI NIE KUPIŁBYŚ UBEZPIECZENIA")
        self.header2.configure(text="JEŚLI KUPIŁBYŚ UBEZPIECZENIE", )
        self.fill_custom_data()

    def fill_custom_data(self, *args):

        punktacja = scenariusze_punkty.get('C')

        for i in range(0, 26):
            Label(self, text=str(punktacja[i]), font=MEDIUM_FONT).grid(row=i + 3, column=5)

        for i in range(0, 25):
            Label(self, text=str(punktacja[26+i]), font=MEDIUM_FONT).grid(row=i + 3, column=7)

    def next_page(self, controller):
        controller.show_frame(QuestionPage3)

    def previous_page(self, controller):
        controller.show_frame(InfoPage3)


class QuestionPage3(QuestionPage):

    def next_page(self, controller):
        if controller.user.scenario.get() != 'D':
            controller.show_frame(InfoPageR)
        else:
            controller.show_frame(InfoPage16)

    def __init__(self, parent, controller, choice_var):
        super().__init__(parent, controller, choice_var)
        rb = Radiobutton(self, text="TAK", font=LARGE_FONT, variable=self.choice_var, value=1, command=self.enable_next_button, indicatoron=0, height=4, width=8).grid(row=2, column=0)
        rb = Radiobutton(self, text="NIE", font=LARGE_FONT, variable=self.choice_var, value=0, command=self.enable_next_button, indicatoron=0, height=4, width=8).grid(row=2, column=1)
        self.st.delete(1.0, END)
        self.st.insert(END, "Odpowiedz teraz, czy hipotetycznie byłbyś skłonny "
                            "wykupić ubezpieczenie od utraty punktów, które kosztuje "
                            "1 punkt przy udziale  własnym 20% (oznacza to, że "
                            "za niezapamiętanie 1 słowa tracisz 0,4 punktu a nie "
                            "2 punkty)?"
                            "\n" "\n"
                            "Jeżeli wykupiłbyś ubezpieczenie i okazałoby się ono "
                            "przydatne musiałbyś złożyć wniosek on-line (mailem) "
                            "o jego zrealizowanie według dostępnego wzoru "
                            "w ciągu 1 tygodnia od zakończenia gry. "
                            "Jeśli tego byś nie zrobił, ubezpieczenie nie zadziałałoby." )

        self.st.configure(state='disabled')

    def previous_page(self, controller):
        controller.show_frame(Table3Page)


class InfoPageR(InfoPage):

    def next_page(self, controller):
        controller.show_frame(TableRPage)

    def previous_page(self, controller):
        controller.show_frame(QuestionPage3)

    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.bind_class("Frame", "<<ScenarioReady3>>", self.set_scenario)

    def set_scenario(self, *args):
        self.st.configure(state='normal')
        self.st.delete(1.0, END)
        self.st.insert(END, R_scenariusze.get(self.controller.user.scenario.get())['info'])
        self.st.configure(state='disabled')


class TableRPage(TablePage):
    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.bind_class("Frame", "<<ScenarioReady>>", self.fill_custom_data)
        self.header1.configure(text="JEŚLI NIE KUPIŁBYŚ UBEZPIECZENIA")
        self.header2.configure(text="JEŚLI KUPIŁBYŚ UBEZPIECZENIE", )

    def fill_custom_data(self, *args):
        punktacja = scenariusze_punkty.get(self.controller.user.scenario.get())

        for i in range(0, 26):
            Label(self, text=str(punktacja[i]), font=MEDIUM_FONT).grid(row=i + 3, column=5)

        for i in range(0, 25):
            Label(self, text=str(punktacja[26+i]), font=MEDIUM_FONT).grid(row=i + 3, column=7)

    def next_page(self, controller):
        controller.show_frame(QuestionPageR)

    def previous_page(self, controller):
        controller.show_frame(InfoPageR)


class QuestionPageR(QuestionPage):
    """ pytanie rzeczywiste dla scenariuszy a-b-c"""
    def next_page(self, controller):
        next_frame = controller.frames[InfoPage16]
        next_frame.show_me()

    def __init__(self, parent, controller, choice_var):
        super().__init__(parent, controller, choice_var)
        self.bind_class("Frame", "<<ScenarioReady2>>", self.set_scenario)
        rb = Radiobutton(self, text="TAK", font=LARGE_FONT, variable=self.choice_var, value=1, command=self.enable_next_button, indicatoron=0, height=4, width=8).grid(row=2, column=0)
        rb = Radiobutton(self, text="NIE", font=LARGE_FONT, variable=self.choice_var, value=0, command=self.enable_next_button, indicatoron=0, height=4, width=8).grid(row=2, column=1)

    def previous_page(self, controller):
        controller.show_frame(TableRPage)

    def set_scenario(self, *args):
        self.st.configure(state='normal')
        self.st.delete(1.0, END)
        self.st.insert(END, R_scenariusze.get(self.controller.user.scenario.get())['question'])
        self.st.configure(state='disabled')


class InfoPage16(InfoPage):

    def next_page(self, controller):
        next_frame = controller.frames[WordsToRemeberPage]
        next_frame.enable_timer()
        next_frame.show_me()

    def previous_page(self, controller):
        controller.show_frame(QuestionPageR)

    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.bind("<<ShowFrame>>", self.fill_text)

    def fill_text(self, *args):
        self.st.configure(state='normal')
        self.st.delete(1.0, END)
        self.st.insert(END, slajd16.get(
            self.controller.user.scenario.get()
        ).get(
            self.controller.user.choiceR.get(), slajd16['D'][0]
        ))

        self.st.configure(state='disabled')


class InfoPage17(InfoPage):

    def next_page(self, controller):
        next_frame = controller.frames[EnterWordsPage]
        next_frame.enable_timer()
        next_frame.show_me()

    def __init__(self, parent, controller):
        super().__init__(parent, controller)
        self.st.delete(1.0, END)
        self.st.insert(END, "Przygotuj się, na kolejnej stronie pojawi się arkusz do wpisania zapamiętanych przez Ciebie słów. Pamiętaj, masz na to najwyżej 10 minut.")
        self.st.configure(state='disabled')

    def previous_page(self, controller):
        controller.show_frame(InfoPage16)


class EmailSender:
    filename = 'FORMULARZ-ZGŁOSZENIA-SZKODY-2022.docx'

    def relative_patch(self, filename):
        bundle_dir = getattr(sys, '_MEIPASS', path.abspath(path.dirname(__file__)))
        path_to_dat = path.abspath(path.join(bundle_dir, filename))
        return path_to_dat

    def send_message_owner(self, user):
        # create message object instance
        msg = MIMEMultipart()
        message = str(user)
        password = "fauvhzhutffprxel"
        msg['From'] = "sonataforlicz@gmail.com"
        msg['To'] = "maria.forlicz@ue.wroc.pl"
        msg['Subject'] = "Eksperyment 1"

        msg.attach(MIMEText(message, 'plain'))
        attachment = MIMEText(user.to_json())
        attachment.add_header('Content-Disposition', 'attachment',
                              filename="{}.json".format(user.album.get()))
        msg.attach(attachment)

        if user.scenario != 'D':
            filename = self.relative_patch(self.filename)
            attachment = open(filename, 'rb')
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())
            encoders.encode_base64(part)
            part.add_header("Content-Disposition",
                            "attachment", filename=self.filename)
            msg.attach(part)

        server = smtplib.SMTP(host='smtp.gmail.com', port=587)
        server.starttls()
        server.login(msg['From'], password)
        server.sendmail(msg['From'], msg['To'], msg.as_string())
        server.quit()

    def send_message_student(self, user):
        msg = MIMEMultipart()
        message = "WZÓR FORMULARZA ZGŁOSZENIA CHĘCI WYPŁATY Z UBEZPIECZENIA DOSTĘPNY W ZAŁĄCZNIKU."
        password = "fauvhzhutffprxel"
        msg['From'] = "sonataforlicz@gmail.com"
        msg['To'] = "{}@student.ue.wroc.pl".format(user.album.get())
        msg['Subject'] = "GRA STATYSTYKA"

        msg.attach(MIMEText(message, 'plain'))

        if user.scenario != 'D':
            filename = self.relative_patch(self.filename)
            attachment = open(filename, 'rb')
            part = MIMEBase("application", "octet-stream")
            part.set_payload(attachment.read())
            encoders.encode_base64(part)
            part.add_header("Content-Disposition",
                            "attachment", filename=self.filename)
            msg.attach(part)

        server = smtplib.SMTP(host='smtp.gmail.com', port=587)
        server.starttls()
        server.login(msg['From'], password)
        server.sendmail(msg['From'], msg['To'], msg.as_string())
        server.quit()

app = StataQuiz()
