zestaw_1 = ['rozum',	'druty',	'śnieg',	'śmieci',	'marmur',
'uczta',	'magik',	'pępek',	'papier',	'szczyt',
'maska',	'broda',	'śledź', 'beczka',	'kasjer',
'sufit',	'enzym',	'dłuto',	'wyścig',	'kryzys',
'ekran',	'ramię',	'powód',	'wybory',	'trumna',
'orzeł',	'flirt',	'gitara', 'zatoka',	'żebrak',
'szyja',	'pasja',	'jagoda', 'zasada',	'lakier',
'zioło',	'krzyk',	'żelazo', 'krytyk',	'stanik',
'album',	'wózek',	'gwóźdź', 'ziemia',	'kurier',
'maszt',	'locha',	'odrzut', 'trwoga',	'perkal']

zestaw_2 = [
    'DRUTY', 'MAGIK', 'GRZYB', 'BONUS', 'WYSPA', 'MOTYL', 'OBAWA', 'ŁYŻKA', 'KAPEĆ', 'HARFA', 'KRATA',
    'NUMER', 'MIECH', 'WIATR', 'KIOSK', 'PAŁAC', 'WSTYD', 'REKIN', 'ANTYK', 'LIMIT', 'CZOŁG',
    'ISKRA', 'KOLOR', 'MAŁPA', 'POWÓZ', 'MORWA', 'FLOTA', 'PALEC', 'KUBEK', 'BASEN', 'KURANT',
    'MRÓWKA', 'ZŁĄCZE', 'KAMIEŃ', 'ZATOKA', 'KOMETA', 'POGODA', 'BIURKO', 'APTEKA', 'PLAKAT',
    'KOMPAS', 'TALERZ', 'OKAZJA', 'KOSTKA', 'KLUCZE', 'ODDECH', 'DYNAMO', 'SZACHY', 'PLACEK',
    'PIKNIK'
]

zestaw_3 = [
    'KOMETA', 'SZORTY', 'TKANKA', 'WYBORY', 'ZAJAZD', 'MARMUR', 'CEBULA', 'SZALIK', 'REGION',
    'KANGUR', 'PODPIS', 'KALEKA', 'TERMIT', 'KOMIKS', 'GWÓŹDŹ', 'GRZMOT', 'BECZKA', 'KARTKA',
    'BABCIA', 'APATIA', 'MARKA', 'TARAS', 'FOTEL', 'OPIUM', 'ZEBRA', 'KAJAK', 'PLECY', 'NORKA',
    'PAJAC', 'PIÓRO', 'ZEGAR', 'ODPAD', 'MIARA', 'RADAR', 'PRASA', 'WINDA', 'BAJKA', 'METRO',
    'AGENT', 'KOGUT', 'DESZCZ', 'CISZA', 'NIEBO', 'PAUZA', 'MYŚLI', 'SERCE', 'RADNY', 'ZRAZY',
    'WSTYD', 'POWÓD'
]

zestaw_4 = ['adres', 'babka', 'cacko', 'dacza', 'edykt', 'facet', 'gacek', 'habit', 'idiom',
            'jacht',
            'kabel', 'lager', 'macki', 'nacja', 'obawa', 'pacan', 'raban', 'sabat', 'tabor',
            'ucisk',
            'wabik', 'zadra', 'afekt', 'badyl', 'cecha', 'dalia', 'efekt', 'fagot', 'galop',
            'haker',
            'iglak', 'jajko', 'kabza', 'lalka', 'mafia', 'nafta', 'obcas', 'pacha', 'rabat',
            'sadza',
            'tabun', 'uczta', 'wacik', 'zakaz', 'afera', 'bagno', 'cekin', 'damka', 'egida',
            'fajka',
            'galon', 'halka', 'igloo', 'jatka', 'kacyk', 'lampa', 'magia', 'nakaz', 'obiad',
            'padok',
            'rabin', 'sakwa', 'tacka', 'ugoda', 'wafel', 'zakon', 'agawa', 'bajka', 'cesja',
            'datek',
            'ekipa', 'fakir', 'ganek', 'halny', 'ikona', 'jazda', 'kadet', 'lanca', 'magma',
            'nalot',
            'obieg', 'pajda', 'racja', 'saldo', 'tafla', 'ukrop', 'wagon', 'zakup', 'agent',
            'balia',
            'cewka', 'dawca', 'ekran', 'fanka', 'garda', 'hamak', 'imbus', 'jawor', 'kadra',
            'lapis',
            'malec', 'napad', 'objaw', 'pajac', 'radar', 'salon', 'tafta', 'ulewa', 'wakat',
            'zalew',
            'agora', 'balon', 'chaos', 'dawka', 'elana', 'farba', 'gazda', 'harce', 'impas',
            'jogin',
            'kafel', 'larwa', 'malwa', 'napar', 'obora', 'palec', 'radca', 'salsa', 'tajga',
            'ulica',
            'walec', 'zamek', 'ajent', 'balsa', 'chart', 'debet', 'elekt', 'farma', 'gazik',
            'harem',
            'impet', 'joker', 'kajak', 'laser', 'mamut', 'napis', 'obraz', 'palik', 'radio',
            'salto',
            'talia', 'umiar', 'walka', 'zamsz', 'akcja', 'banan', 'chata', 'delta', 'elita',
            'farsa',
            'genom', 'harfa', 'indyk', 'jukka', 'kakao', 'laska', 'manat', 'narty', 'obrus',
            'palma',
            'radny', 'salwa', 'talon', 'umowa', 'walor', 'zanik', 'akord', 'banda', 'chlew',
            'demon',
            'enzym', 'fason', 'getry', 'heban', 'iskra', 'junak', 'kalka', 'lasso', 'mania',
            'natka',
            'obrys', 'palto', 'ramka', 'sanki', 'tango', 'uncja', 'wanna', 'zapas', 'akryl',
            'baner',
            'chleb', 'denga', 'epoka', 'fatum', 'getto', 'hebel', 'junta', 'kamea', 'leczo',
            'mango',
            'nauka', 'obuch', 'panda', 'rampa', 'saper', 'taras', 'upust', 'wapno', 'zapis',
            'aktor',
            'bania', 'chlor', 'denko', 'etola', 'fauna', 'gibon', 'henna', 'juror', 'kambr',
            'legar',
            'manna', 'nawis', 'ocean', 'pampa', 'ranek', 'sarna', 'taran', 'uraza', 'warga',
            'zarys',
            'akwen', 'barak', 'chrom', 'derka', 'etyka', 'felga', 'gleba', 'heros', 'jurta',
            'kamyk',
            'legia', 'manta', 'nawyk', 'ocena', 'panel', 'ranga', 'satyr', 'tapir', 'urlop',
            'wazon',
            'zaspa', 'alarm', 'baran', 'chwyt', 'deser', 'fenek', 'glejt', 'hiena', 'kania',
            'lejce',
            'marka', 'nazwa', 'ochra', 'panna', 'raper', 'sauna', 'tarka', 'uroda', 'wdowa',
            'zator',
            'album', 'barek', 'ciecz', 'deska', 'fenol', 'glina', 'hipis', 'kanwa', 'lemur',
            'marsz', 'nerka', 'odlot', 'papka', 'rausz', 'scena', 'tarot', 'urwis', 'welon',
            'zbieg', 'alert', 'barka', 'cisza', 'dieta', 'fetor', 'gmach', 'hobby', 'kapar',
            'lenno', 'maska', 'nerwy', 'odlew', 'papla', 'rebus', 'schab', 'tarta', 'uskok',
            'welur', 'zebra', 'aleja', 'barok', 'cnota', 'dioda', 'fibra', 'gmina', 'hokej',
            'kapok', 'licho', 'masyw', 'niebo', 'odpad', 'pasek', 'rejon', 'seans', 'tasak',
            'utarg', 'werwa', 'zegar', 'alibi', 'baron', 'cudak', 'dobro', 'figle', 'gnida',
            'homar', 'karma', 'lider', 'maszt', 'nieuk', 'odpis', 'pasja', 'rekin', 'sedan',
            'tatar', 'uwaga', 'wiano', 'zefir', 'aloes', 'barwa', 'cugle', 'dorsz', 'fikus',
            'gniew', 'honor', 'karta', 'lilia', 'matka', 'nimfa', 'odwet', 'pasmo', 'remik',
            'sedno', 'teatr', 'wiara', 'zenit', 'amant', 'basen', 'cyfra', 'dotyk', 'filet',
            'gniot', 'horda', 'kasza', 'limfa', 'mazak', 'nisza', 'odwyk', 'passa', 'remis',
            'sekta', 'tekst', 'wiatr', 'zgaga', 'ambra', 'batat', 'cykor', 'draka', 'filia',
            'gogle', 'hossa', 'katar', 'limit', 'mebel', 'nitka', 'ogier', 'pasta', 'renta',
            'serce', 'temat', 'widmo', 'zgoda', 'ameba', 'baton', 'cynik', 'droga', 'filtr',
            'golec', 'hotel', 'kazus', 'lincz', 'media', 'norma', 'ognik', 'pasza', 'rewia',
            'seria', 'tembr', 'widok', 'zgryz', 'aneks', 'bazar', 'cypel', 'drops', 'finka',
            'goryl', 'hucpa', 'kciuk', 'linia', 'melon', 'norka', 'ogrom', 'patio', 'rewir',
            'sesja', 'tempo', 'wieko', 'zguba', 'anoda', 'becik', 'cywil', 'drwal', 'firma',
            'gracz', 'humor', 'kefir', 'linka', 'menel', 'norma', 'ohyda', 'patos', 'rezon',
            'sezam', 'tenis', 'wigor', 'zimno', 'anons', 'bejca', 'czapa', 'drzwi', 'fizyk',
            'grant', 'hycel', 'kibic', 'liryk', 'metal', 'notes', 'okapi', 'pauza', 'robak',
            'sfera', 'tenor', 'wilga', 'zjawa', 'antyk', 'bekon', 'czara', 'dukat', 'flaga',
            'grill', 'kiesa', 'lista', 'metan', 'numer', 'olcha', 'pazur', 'robot', 'sfora',
            'teren', 'willa', 'zjazd', 'aorta', 'beksa', 'czary', 'dumka', 'flara', 'grono',
            'kijek', 'lizak', 'metka', 'nylon', 'oliwa', 'peron', 'rodak', 'sierp', 'terma',
            'winda', 'zmora', 'arbuz', 'belka', 'fleja', 'grosz', 'kikut', 'lizus', 'metro',
            'omlet', 'piach', 'rogal', 'silos', 'tetra', 'wirus', 'zmowa', 'arena', 'berek',
            'flesz', 'grota', 'kilof', 'lobby', 'miano', 'opcja', 'picie', 'rolka', 'sitko',
            'tiara', 'wizja', 'zmrok', 'argon', 'bessa', 'flirt', 'groza', 'kiosk', 'locha',
            'miara', 'opera', 'piegi', 'rondo', 'sitwa', 'tkacz', 'wjazd', 'znicz', 'armia',
            'betka', 'flora', 'gruda', 'kiper', 'lokaj', 'miech', 'opium', 'pierz', 'rower',
            'skala', 'toast', 'wojak', 'zorza', 'arras', 'beton', 'flota', 'grunt', 'klapa',
            'lotka', 'miecz', 'opoka', 'pigwa', 'rozum', 'skarb', 'tomik', 'wojna', 'zrost',
            'arsen', 'bidon', 'fluid', 'grupa', 'klasa', 'lwica', 'minus', 'opona', 'pijak',
            'ruina', 'skaza', 'toner', 'worek', 'zrzut', 'astma', 'bieda', 'fobia', 'gryka',
            'kleik', 'mirra', 'optyk', 'pilot', 'rulon', 'skaut', 'tonik', 'wrona', 'zwiad',
            'atest', 'bigos', 'fluor', 'grypa', 'kleks', 'misja', 'order', 'pinia', 'rumak',
            'skecz', 'topaz', 'wrzos', 'zwrot', 'atlas', 'bigot', 'folia', 'gryps', 'klika',
            'miska', 'organ', 'pinta', 'rumba', 'sklep', 'torba', 'wujek', 'zydel', 'autor',
            'bitwa', 'fonia', 'grzyb', 'klips', 'mleko', 'orkan', 'pirat', 'runda', 'slang',
            'totem', 'wydma', 'awans', 'biuro', 'forma', 'guano', 'klops', 'mnich', 'osada',
            'pisak', 'rurka', 'slajd', 'towar', 'wydra', 'awers', 'biwak', 'forsa', 'gumka',
            'klucz', 'moher', 'osika', 'pismo', 'ruszt', 'sonda', 'trakt', 'wyjec', 'awizo',
            'bizon', 'forum', 'guzik', 'kmiot', 'mores', 'osoba', 'plaga', 'rybak', 'sonar',
            'trans', 'wylot', 'blask', 'fotel', 'gwara', 'koala', 'morwa', 'otoka', 'plama',
            'rygor', 'sonet', 'trasa', 'wynik', 'bluza', 'foton', 'gwint', 'kobra', 'morze',
            'owies', 'plebs', 'ryjek', 'sopel', 'trawa', 'wypad', 'bobas', 'fraza', 'gwizd',
            'kobza', 'motel', 'plecy', 'sosna', 'trend', 'wypis', 'bolec', 'fresk', 'gyros',
            'kocur', 'motor', 'plisa', 'spazm', 'trema', 'wyrok', 'bomba', 'fucha', 'gzyms',
            'kocyk', 'motto', 'pniak', 'splot', 'trias', 'wyrwa', 'bonus', 'furia', 'kojec',
            'motyl', 'poeta', 'spryt', 'trzon', 'wyspa', 'brama', 'fuzja', 'kojot', 'motyw',
            'poker', 'sroka', 'tubka', 'wywar', 'broda', 'kokon', 'mucha', 'polot', 'stado',
            'tukan', 'wymaz', 'bryja', 'kokos', 'mufka', 'pomoc', 'start', 'tuman', 'wzrok',
            'bryza', 'kolba', 'mumia', 'pompa', 'stopa', 'tunel', 'bubel', 'kolec', 'mural',
            'popis', 'stres', 'tuner', 'budka', 'kolej', 'muzyk', 'popyt', 'strop', 'tusza',
            'bufet', 'kolia', 'myjka', 'poryw', 'stypa', 'tytan', 'bulwa', 'kolka', 'posag',
            'sukno', 'bursa', 'kolor', 'potas', 'susza', 'burta', 'komar', 'potop', 'swada',
            'burza', 'komik', 'pozew', 'syfon', 'butik', 'komin', 'praca', 'syrop', 'butla',
            'komis', 'pracz', 'szafa', 'buzia', 'konar', 'prasa', 'szala', 'konik', 'prawo',
            'szata', 'konto', 'proca', 'szejk', 'koper', 'proso', 'szewc', 'kopia', 'proza',
            'szkic', 'koral', 'psota', 'szlak', 'korba', 'puder', 'szlam', 'korek', 'pudel',
            'szlif', 'koszt', 'pupil', 'sznur', 'kotwa', 'puzon', 'szopa', 'kozak', 'pycha',
            'szosa', 'kowal', 'szpak', 'kpina', 'szpic', 'krach', 'szpik', 'krasa', 'sztos',
            'krata', 'szyba', 'kreda', 'szyfr', 'kreza', 'szyja', 'krowa', 'szyna', 'krzak',
            'szypr', 'krzem', 'krzew', 'krzta', 'krzyk', 'ksero', 'kubek', 'kufel', 'kufer',
            'kujon', 'kulig', 'kulka', 'kumak', 'kupon', 'kuria', 'kurka', 'kutia', 'kuzyn',
            'kwarc', 'kwiat', 'kwoka', 'kwota']


delta2 = (38-28)/50
delta3 = (39-19)/50

wynik_koncowy_1 = [37 for i in range(0, 51)]
wynik_koncowy_2 = [round(38 - i*delta2, 1) for i in range(0, 51)]
wynik_koncowy_3 = [round(39 - i*delta3, 1) for i in range(0, 51)]
wynik_koncowy_4 = [0 for i in range(0, 51)]
scenariusze_punkty = {
    'A': wynik_koncowy_1,
    'B': wynik_koncowy_2,
    'C': wynik_koncowy_3,
    'D': wynik_koncowy_4
}

R_scenario_A_infopage = "Zastanów się czy chcesz wykupić ubezpieczenie od utraty punktów, które " \
                      "kosztuje 3 punkty i w pełni chroni Cię przed utratą punktów w razie niezapamiętania " \
                      "dowolnej liczby słów? Wybierając grę z tym ubezpieczeniem zawsze otrzymasz 37 punktów, niezależnie od tego, ile słów zapamiętasz. Przykładowo: jeśli nie zapamiętasz 10 słów to grając z tym ubezpieczeniem stracisz 0 punktów, grając bez ubezpieczenia starcisz 20 punktów." \
                      "Na następnym slajdzie zobaczysz, jak kształtowałaby się liczba Twoich punktów w sytuacji, " \
                      "gdy nie wykupisz ubezpieczenia i gdybyś go wykupił. Potem odpowiesz na zadane pytanie." \
                      "\n" "\n" \
                      "Jeżeli wykupisz ubezpieczenie i okaże się ono przydatne będziesz " \
                      "musiał złożyć wniosek on-line (mailem) o jego zrealizowanie " \
                      "według dostępnego wzoru w ciągu 1 tygodnia od zakończenia gry " \
                      "Jeśli tego nie zrobisz, ubezpieczenie nie zadziała.Kwota za ubezpieczenie "\
                      "(jeśli się na nie zdecydujesz) zostanie pobrana niezależnie od tego, czy zgłosisz "\
                      "szkodę przesyłając formularz."

R_scenario_A_questionpage = "Odpowiedz teraz, czy chcesz wykupić ubezpieczenie od " \
                          "utraty punktów, które kosztuje 3 punkty i w pełni " \
                          "chroni Cię przed utratą punktów w razie niezapamiętania " \
                          "dowolnej liczby słów? " \
                          "JEST TO JEDYNA OFERTA UBEZPIECZENIA JAKĄ OTRZYMASZ" \
                          "\n" "\n"  \
                            "Przypominamy, jeżeli wykupisz ubezpieczenie i okaże " \
                          "się ono przydatne będziesz musiał złożyć wniosek on-line " \
                          "(mailem) o jego zrealizowanie według dostępnego wzoru w ciągu " \
                          "1 tygodnia od zakończenia gry " \
                          "Jeśli tego nie zrobisz, ubezpieczenie nie zadziała. Kwota za ubezpieczenie "\
                          "(jeśli się na nie zdecydujesz) zostanie pobrana niezależnie od tego, czy zgłosisz "\
                          "szkodę przesyłając formularz."

R_scenario_B_infopage = "Zastanów się czy chcesz wykupić ubezpieczenie od utraty punktów, które " \
                        "kosztuje 2 punkty przy udziale  własnym 10% (oznacza to, że za niezapamiętanie " \
                        "1 słowa tracisz 0,2 punktu a nie 2 punkty)?  Przykładowo: jeśli nie zapamiętasz 10 słów to grając z tym ubezpieczeniem stracisz 2 punkty, grając bez ubezpieczenia starcisz 20 punktów." \
                         "Na następnym slajdzie zobaczysz, " \
                        "jak kształtowałaby się liczba Twoich punktów w sytuacji, " \
                        "gdy nie wykupisz ubezpieczenia i gdybyś go wykupił. Potem odpowiesz na zadane pytanie." \
                        "\n" "\n" \
                        "Jeżeli wykupisz ubezpieczenie i okaże się ono przydatne będziesz " \
                        "musiał złożyć wniosek on-line (mailem) o jego zrealizowanie " \
                        "według dostępnego wzoru w ciągu 1 tygodnia od zakończenia gry " \
                        "Jeśli tego nie zrobisz, ubezpieczenie nie zadziała. Kwota za ubezpieczenie "\
                      "(jeśli się na nie zdecydujesz) zostanie pobrana niezależnie od tego, czy zgłosisz "\
                      "szkodę przesyłając formularz."

R_scenario_B_questionpage = "Odpowiedz teraz, czy chcesz wykupić ubezpieczenie od utraty punktów, które kosztuje 2 punkty "\
                            "przy udziale  własnym 10% (oznacza to, że za niezapamiętanie 1 słowa tracisz 0,2 punktu a nie 2 punkty)? " \
                            "JEST TO JEDYNA OFERTA UBEZPIECZENIA JAKĄ OTRZYMASZ" \
                            "\n" "\n" \
                            "Przypominamy, jeżeli wykupisz ubezpieczenie i okaże " \
                            "się ono przydatne będziesz musiał złożyć wniosek on-line " \
                            "(mailem) o jego zrealizowanie według dostępnego wzoru w ciągu " \
                            "1 tygodnia od zakończenia gry " \
                            "Jeśli tego nie zrobisz, ubezpieczenie nie zadziała.Kwota za ubezpieczenie "\
                      "(jeśli się na nie zdecydujesz) zostanie pobrana niezależnie od tego, czy zgłosisz "\
                      "szkodę przesyłając formularz."


R_scenario_C_infopage = "Zastanów się czy chcesz wykupić ubezpieczenie od utraty punktów, które " \
                        "kosztuje 1 punkt przy udziale  własnym 20% (oznacza to, że za niezapamiętanie " \
                        "1 słowa tracisz 0,4 punktu a nie 2 punkty)?  Przykładowo: jeśli nie zapamiętasz 10 słów to grając z tym ubezpieczeniem stracisz 4 punkty, grając bez ubezpieczenia starcisz 20 punktów." \
                         "Na następnym slajdzie zobaczysz," \
                        "jak kształtowałaby się liczba Twoich punktów w sytuacji," \
                        "gdy nie wykupisz ubezpieczenia i gdybyś go wykupił. Potem odpowiesz na zadane pytanie." \
                        "\n" "\n" \
                        "Jeżeli wykupisz ubezpieczenie i okaże się ono przydatne będziesz " \
                        "musiał złożyć wniosek on-line (mailem) o jego zrealizowanie " \
                        "według dostępnego wzoru w ciągu 1 tygodnia od zakończenia gry " \
                        "Jeśli tego nie zrobisz, ubezpieczenie nie zadziała.Kwota za ubezpieczenie "\
                      "(jeśli się na nie zdecydujesz) zostanie pobrana niezależnie od tego, czy zgłosisz "\
                      "szkodę przesyłając formularz."

R_scenario_C_questionpage = "Odpowiedz teraz, czy chcesz wykupić ubezpieczenie od utraty punktów, które kosztuje 1 punkt "\
                            "przy udziale  własnym 20% (oznacza to, że za niezapamiętanie 1 słowa tracisz 0,4 punktu a nie 2 punkty)? " \
                            "JEST TO JEDYNA OFERTA UBEZPIECZENIA JAKĄ OTRZYMASZ" \
                            "\n" "\n" \
                            "Przypominamy, jeżeli wykupisz ubezpieczenie i okaże " \
                            "się ono przydatne będziesz musiał złożyć wniosek on-line " \
                            "(mailem) o jego zrealizowanie według dostępnego wzoru w ciągu " \
                            "1 tygodnia od zakończenia gry " \
                            "Jeśli tego nie zrobisz, ubezpieczenie nie zadziała. Kwota za ubezpieczenie "\
                      "(jeśli się na nie zdecydujesz) zostanie pobrana niezależnie od tego, czy zgłosisz "\
                      "szkodę przesyłając formularz."

R_scenariusze = {
    'A': {'info': R_scenario_A_infopage, 'question': R_scenario_A_questionpage},
    'B': {'info': R_scenario_B_infopage, 'question': R_scenario_B_questionpage},
    'C': {'info': R_scenario_C_infopage, 'question': R_scenario_C_questionpage},
}

slajd16_info_abc = {
    1: "Wybrałeś grę z ubezpieczeniem. Jesteś ubezpieczony. Na kolejnym slajdzie pojawi się 50 słów do zapamiętania. Pamiętaj, masz najwyżej 60 minut na ich zapamiętanie. Jeżeli skończysz grę przed czasem możesz opuścić spotkanie. UWAGA, gdy przewiniesz slajd ze słowami nie będziesz mógł do niego wrócić!",
    0: "Wybrałeś grę bez ubezpieczenia. Nie jesteś ubezpieczony. Na kolejnym slajdzie pojawi się 50 słów do zapamiętania. Pamiętaj, masz najwyżej 60 minut na ich zapamiętanie. Jeżeli skończysz grę przed czasem możesz opuścić spotkanie. UWAGA, gdy przewiniesz slajd ze słowami nie będziesz mógł do niego wrócić!"
    }
slajd16_info_d = "Na kolejnym slajdzie pojawi się 50 słów do zapamiętania. Pamiętaj, że grasz bez ubezpieczenia i masz najwyżej 60 minut na ich zapamiętanie. Jeżeli skończysz grę przed czasem możesz opuścić spotkanie. UWAGA, gdy przewiniesz slajd ze słowami nie będziesz mógł do niego wrócić!"

slajd16 = {
    'A': slajd16_info_abc,
    'B': slajd16_info_abc,
    'C': slajd16_info_abc,
    'D': {1: slajd16_info_d, 0: slajd16_info_d}
}

sentABC_text = "Twoj wynik to: {} słów poprawnie zapamietanych. Pamiętaj, jeśli się ubezpieczyłeś i chcesz skorzystać z ubezpieczenia musisz złożyć wniosek on-line na adres maria.forlicz@ue.wroc.pl.\n \
            DZIĘKUJEMY ZA UDZIAŁ W EKSPERYMENCIE!"
sentD_text = "Twój wynik to {} zapamiętanych słów. Zachowałeś {} punktów.\n \
            DZIĘKUJEMY ZA UDZIAŁ W EKSPERYMENCIE!"


slajd_sent = {
    'A': sentABC_text,
    'B': sentABC_text,
    'C': sentABC_text,
    'D': sentD_text
}

mapa_podzial = {"A": 0, "B": 1, "C": 2, "D": 3}

import hashlib

passwd = ['BNK297B',
          'ABY128Q',
          "KSW753W",
          'PES294R',
          'FGA175J',
          'EDM379K',
          'NTW591G',
          'UTR673W',
          'PLH290L',
          'YRC531K'
          ]
zestawy = [zestaw_1, zestaw_2, zestaw_3]

podzial = {
181748	: 'D',
178350	: 'C',
181863	: 'A',
181222	: 'B',
181826	: 'B',
182115	: 'A',
181929	: 'D',
181443	: 'B',
182300	: 'C',
181636	: 'A',
181707	: 'B',
182538	: 'C',
182070	: 'D',
182456	: 'A',
182165	: 'D',
181462	: 'D',
177338	: 'B',
181300	: 'C',
181761	: 'A',
181500	: 'B',
182131	: 'C',
181687	: 'D',
182544	: 'A',
182099	: 'B',
182137	: 'C',
182444	: 'D',
182088	: 'A',
181452	: 'A',
182284	: 'B',
181296	: 'C',
182560	: 'D',
181542	: 'A',
181952	: 'B',
181948	: 'C',
181456	: 'D',
182074	: 'D',
182033	: 'B',
166281	: 'C',
182201	: 'B',
181750	: 'C',
181576	: 'A',
182470	: 'A',
182635	: 'A',
181850	: 'A',
182095	: 'B',
181384	: 'B',
181360	: 'B',
181765	: 'C',
182588	: 'C',
181919	: 'C',
182357	: 'D',
181440	: 'D',
181990	: 'D',
181746	: 'D',
181951	: 'A',
181229	: 'B',
181741	: 'C',
181278	: 'D',
182205	: 'A',
181600	: 'B',
181696	: 'C',
182363	: 'D',
182325	: 'A',
181888	: 'D',
182425	: 'C',
181275	: 'D',
181538	: 'D',
181688	: 'D',
182438	: 'A',
182047	: 'B',
181463	: 'C',
181999	: 'D',
177441	: 'A',
182369	: 'B',
182014	: 'C',
181813	: 'D',
181873	: 'A',
182581	: 'B',
181507	: 'C',
182299	: 'D',
181444	: 'A',
181767	: 'B',
182267	: 'C',
182572	: 'D',
182313	: 'D',
181691	: 'A',
181504	: 'B',
182010	: 'C',
181784	: 'D',
181539	: 'D',
181711	: 'C',
182419	: 'A',
181453	: 'B',
181572	: 'A',
181785	: 'B',
181446	: 'C',
182289	: 'D',
182001	: 'D',
181340	: 'B',
181586	: 'D',
181751	: 'D',
181263	: 'A',
181708	: 'B',
181677	: 'C',
182262	: 'D',
181256	: 'A',
181939	: 'B',
181928	: 'C',
181354	: 'D',
181489	: 'A',
182231	: 'B',
182440	: 'C',
182085	: 'D',
182627	: 'A',
182332	: 'B',
182318	: 'C',
181588	: 'A',
182240	: 'D',
181830	: 'A',
181864	: 'C',
181753	: 'D',
181252	: 'B',
182323	: 'A',
177611	: 'D',
181732	: 'C',
181867	: 'D',
182468	: 'A',
182148	: 'B',
181508	: 'C',
181509	: 'D',
181829	: 'D',
181846	: 'D',
181897	: 'A',
182580	: 'B',
181359	: 'D',
181985	: 'C',
182257	: 'A',
181423	: 'B',
181806	: 'C',
182349	: 'C',
182195	: 'D',
181833	: 'B',
181634	: 'C',
181544	: 'D',
178361	: 'A',
181992	: 'B',
181671	: 'D',
181804	: 'C',
181989	: 'A',
181763	: 'B',
182410	: 'D',
181418	: 'A',
181814	: 'C',
177424	: 'C',
181321	: 'A',
181295	: 'B',
175239	: 'C',
181416	: 'D',
182037	: 'A',
181626	: 'B',
181335	: 'C',
181961	: 'D',
182594	: 'A',
181700	: 'B',
182091	: 'C',
182078	: 'D',
181764	: 'A',
181254	: 'B',
181648	: 'B',
182386	: 'D',
182334  : 'D'
}
